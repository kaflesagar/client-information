<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientStoreTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        // $this->assertTrue(true);
    }

    public function testNewClient(){
    	$this->visit('/')
         ->type('Jimmy', 'name')
         ->type('Kathmandu', 'address')
         ->type('9841414141', 'phone')
       	 ->select('male', 'gender')
         ->type('test@example.com', 'email')
         ->type('Nepali', 'nationality')
         ->type('12/12/1995', 'dob')
         ->select('email','mode_of_contact')
         ->type('Bachelor', 'education_background')
         ->press('Add');
    }
}
