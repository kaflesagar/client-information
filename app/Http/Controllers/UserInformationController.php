<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserValidationRequest;
use App\Http\Requests;
use Wilgucki\Csv\Facades\Reader;
use Wilgucki\Csv\Facades\Writer;

class UserInformationController extends Controller
{
	function __construct()
    {
        $this->csv_file_path = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'UserInformation.csv';

    }
		#User Information Storing form redirected from here
       function create($id = null)
	    {
	       
	        return view('user_info_form');
	    }

	    #User Information Storing Procedure
    	function store(UserValidationRequest $request)
	    {
	    	try {
		// dd($request->toArray());
		        #csv file reading goes here
		        $csv_reader = Reader::open($this->csv_file_path);
		        $data = $csv_reader->readAll();
		        $counter = count($data);
		        $csv_reader->close();

		        #csv file writing goes here
		        $csv_writer = Writer::create($this->csv_file_path);
		        $csv_writer->writeLine([
		            $counter + 1,
		            $request->input('name'),
		            $request->input('address'),
		            $request->input('gender'),
		            $request->input('phone'),
		            $request->input('email'),
		            $request->input('nationality'),
		            $request->input('dob'),
		            $request->input('education_background'),
		            $request->input('mode_of_contact')]);
		        $csv_writer->writeAll($data);
		        $csv_writer->flush();
		        $csv_writer->close();
		        $name = $request->input('name');
		        return redirect()->route('show_user_form')->with('success','Data Inserted Sucessfully!!');

				}
				catch (\Exception $e) {
				    return $e->getMessage();
				}
	    	


	    }

	#show user record
    function showAllUser()
    {
        $csv_reader = Reader::open($this->csv_file_path);
        $datas = $csv_reader->readAll();
        return view('user_info_view', compact('datas'));
    }

    function details($id){
    	$detail_data = [];
    	 $csv_reader = Reader::open($this->csv_file_path);
            $datas = $csv_reader->readAll();
            foreach ($datas as $data) {
                if ($data[0] == $id) {
                    $detail_data = [
                        $data[0],
                        $data[1],
                        $data[2],
                        $data[3],
                        $data[4],
                        $data[5],
                        $data[6],
                        $data[7],
                        $data[8],
                        $data[9]
                    ];
                }

            }
            if (!$detail_data) {
                return '<h3 style="text-align: center" class="mt-6">OOPS given id is not valid.</h3>';
            }
             return view('user_info_details', compact('detail_data'));
    }

}
