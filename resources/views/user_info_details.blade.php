@include('includes/header')
<div class="container">
    <a href="{{route('show_user_form')}}" class="btn btn-primary btn-sm mt-2 mb-3">Add User Record</a>
    <table class="table table-striped" id="viewAllUser">
        <tr>
            <th>Id</th>
            <td>{{$detail_data[0]}}</td>
        </tr>
         <tr>
            <th>Name</th>
            <td>{{$detail_data[1]}}</td>
        </tr>
         <tr>
            <th>Address</th>
            <td>{{$detail_data[2]}}</td>
        </tr>
         <tr>
            <th>Gender</th>
            <td>{{$detail_data[3]}}</td>
        </tr>
         <tr>
            <th>Phone</th>
            <td>{{$detail_data[4]}}</td>
        </tr>
         <tr>
            <th>Email</th>
            <td>{{$detail_data[5]}}</td>
        </tr>
         <tr>
           <th>Nationality</th>
            <td>{{$detail_data[6]}}</td>
        </tr>
         <tr>
            <th>Date of birth</th>
            <td>{{$detail_data[7]}}</td>
        </tr>
         <tr>
            <th>Education Background</th>
            <td>{{$detail_data[8]}}</td>
        </tr>
        <tr>
            <th>Mode of Contact</th>
            <td>{{$detail_data[9]}}</td>
        </tr>

    </table>
        <a href="{{route('view_all_user')}}" class="btn btn-primary btn-sm mt-2 mb-3">Back To List</a>

</div>
@include('includes/footer')